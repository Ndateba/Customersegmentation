﻿Machine learning- Group 17
Title: Customer Segmentation 
Section 1: Introduction
The goal of this project is to identify the optimal number of segments and assign each user to one of them to facilitate targeted marketing campaigns
Section2: Methodology
In this section we are going to describe the methods and steps taken in this project.
Data cleaning:
We first loaded our dataset which is customer segmentation using pandas and then checked whether our dataset has missing values and we found that there are no missing values in the dataset. We used data.isna().sum to check for missing values. We also checked for outliers depending on the price variable but we decided to keep them.


Customer segmentation method:
RFM analysis (Recency, Frequency, Monetary)& cluster analysisRFM analysis can help identify the most valuable customers based on their purchase behavior, while cluster analysis can help uncover patterns and segments in the data and then categorize them. We used 4 categories: At-Risk Customers, Potential Loyalists Customers, New Customers and Loyal
Customers.


Section 3: Data analysis and data visualization
We did descriptive analysis to analyze each numerical variable in our dataset. Then we did like multivariate analysis to assess the correlation among variables we have in our dataset. We did a histogram for single variables like for customer states to see how customers are distributed among states. We also decided to create a pie-chart of payment categories to assess which payment method is used the most.


Section 4: Feature Engineering
Creating new features to be used for recency, frequency and monetary value. For recency we used order_purchase_timestamp for creating a recency feature(variable) which is the number of days assigned to customers of their last purchase. 
Secondly we created a frequency feature which is the number of customers purchased. We extracted this feature from the variable of customer_id and counted them(using .cumcount).


The other feature to create is monetary value which is the sum of spending of each customer. We extracted this feature from combining customer_id and payment_value and transforming by sum.
Section 5: Results of segments created and discussion:
1. We checking the accurate number of segments
We used elbow curve 
  

We decided to use 4 segments.












2. Segments interpretation
  



  

3. Analysis of segment created
We did an analysis based on the revenue contribution of each segment.  We used a pie-chart to assess the percentage of contribution.
  

INTERPRETATION: Results show that at-risk customers have the highest percentage.while loyal have the lowest. 


SECTION 6: CONCLUSION AND RECOMMENDATION
Based on Analysis of our segments created from customers we can recommend the company on how to treat the customers in each segment. for all can be to collect more data, while for each they are as follows.
1. at-risks customers:
1.Develop a loyalty program with tiered benefits
2.Offer exclusive discounts or promotions for at-risks customers
3. Personalize their experience through targeted marketing campaigns as they generate more income to the company.


2.    potential loyal customers:
1.Send personalized emails or offers to encourage them to return and make a purchase
2.Understand the reasons behind their decreased frequency and address any concerns
3.Offer incentives or rewards for repeat purchases to increase their engagement




3.    new customers:
1. Send a welcome email or offer to thank them for their business
2. Encourage them to make another purchase through personalized recommendations
3. Provide a seamless onboarding experience and address any questions or concerns


4. Loyal customers:
1. Reward their loyalty with exclusive discounts or promotions
2. Personalize their experience through targeted marketing campaigns
3.  Encourage referrals or word-of-mouth recommendations






SECTION 7: Contribution of group member:
In this project we collaborated as we did all parts of the project together in person and online from the starting phase to the end. 


Niyonshuti Valentin(771761): Designed the project plan of our project.Implemented and evaluated the RFM Method and clustering. designed the insights of the results. Responsible for making the presentation.
 
Eddy Ndateba Mandela(771351):Created plots and charts to visualize the data and the model performance.  Responsible for data preprocessing(cleaning) and Developed code for cleaning the data and extracting new features. Responsible for writing the technical report


The project was easy and possible as a result of each and everyone's contribution.